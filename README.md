# Stop administrator login

This is a very lightweight module that will stop users from being able to
login as user 1 and optionally block the entire "administrator" role. Site
administrators will still be able to login as user 1 by using drush when
needed. The idea behind this is to protect a site from the accidental loss
of user 1's password. Also from a security perspective (not sharing passwords)
and for auditing configuration/content changes it is much better if user 1 is not used.

This module is a great companion when you are using an IAM like OpenID Connect.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/stop_admin).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/stop_admin).


## Table of contents

- Requirements
- Similar modules
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Similar modules

- [Paranoia](https://www.drupal.org/project/paranoia)
- [Admin denied](https://www.drupal.org/project/admin_denied)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module by default only blocks User 1 from logging in with the UI. To ensure
that users with the site administrator role are also unable to log in, navigate
to `/admin/config/people/stop_admin`, check the 'Block administrator role' box,
and save.

It is possible to override the configuration check if you are for example in a
situation where you still want to be able to login as user 1 on your DEV/STAG
server. Please refer to the documentation page on drupal.org:
[custom-logic-for-disabling](https://www.drupal.org/docs/8/modules/stop-administrator-login/custom-logic-for-disabling)


## Troubleshooting

If you encounter issues please create an issue in the issue queue at:
[stop_admin](http://drupal.org/project/issues/stop_admin).
For the full module documentation please see:
[stop-administrator-login](https://www.drupal.org/docs/8/modules/stop-administrator-login)


## Maintainers

- Bram Driesen - [BramDriesen](https://www.drupal.org/u/bramdriesen)
