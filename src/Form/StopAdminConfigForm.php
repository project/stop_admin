<?php

namespace Drupal\stop_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure settings for the Stop administrator login module.
 */
class StopAdminConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'stop_admin.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stop_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $administrator_role_url = Url::fromRoute('user.role.settings');
    $password_reset_url = Url::fromRoute('user.pass');
    $form['block_admin_role'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Block administrator role'),
      '#default_value' => $config->get('block_admin_role'),
      '#description' => $this->t('When this box is checked, users with the <a href=":administrator_role_url">Administrator role</a> will also be prevented from logging in through the admin UI. Administrators can login with a one-time login link in the email sent by the <a href=":password_reset_url">password recovery form</a>, or by using generating a one-time login link with <code>drush uli --name "[USERNAME]"</code>.', [
        ':administrator_role_url' => $administrator_role_url->toString(),
        ':password_reset_url' => $password_reset_url->toString(),
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('block_admin_role', $form_state->getValue('block_admin_role'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
