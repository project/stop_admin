<?php

/**
 * @file
 * Module file for the Stop administrator module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Implements hook_help().
 */
function stop_admin_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.stop_admin':
      return t('
        <h2>Stop administrator login module for Drupal.</h2>
        <p>This module prevents user 1 from logging into the site via normal means.</p>
        <p>Note: User 1 will still be able to be logged in via drush.</p>
        <h2>WARNING</h2>
        <p>If you enable this module, and do not have access to the machine using <strong>drush</strong> you will NOT, I repeat, NOT be able to log back in as user 1.</p>
      ');
  }
}

/**
 * Implements hook_form_alter().
 */
function stop_admin_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $disabled = Drupal::config('stop_admin.settings')->get('disabled');
  // When using the login form or block, we add a validate callback.
  if (!$disabled && ($form_id === 'user_login_form' || $form_id === 'user_login_block')) {
    $form['#validate'][] = '_stop_admin_prevent_admin_login';
  }
}

/**
 * Callback function to validate the login form.
 */
function _stop_admin_prevent_admin_login(&$form, FormStateInterface &$form_state) {
  if ($uid = $form_state->get('uid')) {
    // Check if the user is user 1.
    if ((int) $uid === 1) {
      // Same message as in UserLoginForm::validateFinal().
      $form_state->setErrorByName('name', t('Unrecognized username or password. <a href=":password">Forgot your password?</a>', [':password' => Url::fromRoute('user.pass')->toString()]));
    }
    // Else check if users with the admin role should be blocked.
    elseif (Drupal::config('stop_admin.settings')->get('block_admin_role')) {
      // First, the admin role needs to be determined.
      $admin_role = NULL;
      // Loop through each of the roles on the site.
      foreach (Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple() as $role) {
        // Determine if the role is the admin role.
        if ($role->status() && $role->isAdmin()) {
          // This is the admin role, so store the reference and exit the loop.
          $admin_role = $role;
          break;
        }
      }

      // Determine if the admin role was found.
      if (!is_null($admin_role)) {
        // Determine if the user logging in has the admin role.
        if (User::load($uid)->hasRole($admin_role->id())) {
          // The user logging in has the admin role, so they are prevented from
          // logging in through the UI.
          $form_state->setErrorByName('name', t('Unrecognized username or password. <a href=":password">Forgot your password?</a>', [':password' => Url::fromRoute('user.pass')->toString()]));
        }
      }
    }
  }
}
