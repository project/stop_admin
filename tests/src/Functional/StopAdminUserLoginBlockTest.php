<?php

namespace Drupal\Tests\stop_admin\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Defines tests for StopAdminUserLoginBlock.
 *
 * @package Drupal\Tests\stop_admin\Functional
 * @group stop_admin
 */
class StopAdminUserLoginBlockTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['block', 'stop_admin'];

  /**
   * A non-administrator user for this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $regularUser;

  /**
   * A administrator user for this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create a regular user and activate it.
    $this->regularUser = $this->drupalCreateUser();
    $this->drupalLogin($this->regularUser);
    $this->drupalLogout();

    // Create a administrator user and activate it.
    $this->adminUser = $this->createUser([], 'superadmin', TRUE);
    $this->drupalLogin($this->adminUser);
    $this->drupalLogout();

    // Place the block.
    $this->placeBlock('user_login_block');
  }

  /**
   * Test the user login block as user 1.
   */
  public function testUserLoginBlockUserOne() {
    $this->drupalGet('node');
    $edit = [
      'name' => $this->rootUser->getAccountName(),
      'pass' => $this->rootUser->passRaw,
    ];
    $this->submitForm($edit, 'Log in');
    $this->assertTrue(strpos($this->getSession()->getPage()->getContent(), (string) 'Log in') > 0, 'User is not logged in.');
    $this->assertSession()->pageTextContains('Unrecognized username or password. Forgot your password?', 'Error message appeared.');
  }

  /**
   * Test the user login block as an authenticated user.
   */
  public function testUserLoginBlockUserAuthenticated() {
    $this->drupalGet('node');
    $edit = [
      'name' => $this->regularUser->getAccountName(),
      'pass' => $this->regularUser->passRaw,
    ];
    $this->submitForm($edit, 'Log in');
    $this->assertFalse(strpos($this->getSession()->getPage()->getContent(), (string) 'Log in') > 0, 'User is logged in.');
    $this->assertSession()->pageTextNotContains('Unrecognized username or password. Forgot your password?', 'Error message did not appear.');
  }

  /**
   * Test the user login block as user 1 when the setting is set to disabled.
   */
  public function testStopAdminDisabledUserLoginBlockUserOne() {
    // Disable the setting.
    \Drupal::configFactory()
      ->getEditable('stop_admin.settings')
      ->set('disabled', TRUE)
      ->save(TRUE);
    // Test to make sure we can login as user 1.
    $this->drupalGet('node');
    $edit = [
      'name' => $this->rootUser->getAccountName(),
      'pass' => $this->rootUser->passRaw,
    ];
    $this->submitForm($edit, 'Log in');
    $this->assertFalse(strpos($this->getSession()->getPage()->getContent(), (string) 'Log in') > 0, 'User is logged in.');
    $this->assertSession()->pageTextNotContains('Unrecognized username or password. Forgot your password?', 'Error message did not appear.');
  }

  /**
   * Test that with the setting disabled admin users can sign in.
   */
  public function testStopAdminRoleSettingDisabledAdminUser() {
    $this->drupalGet('node');
    $edit = [
      'name' => $this->adminUser->getAccountName(),
      'pass' => $this->adminUser->passRaw,
    ];
    $this->submitForm($edit, 'Log in');
    $this->assertFalse(strpos($this->getSession()->getPage()->getContent(), (string) 'Log in') > 0, 'User is logged in.');
    $this->assertSession()->pageTextNotContains('Unrecognized username or password. Forgot your password?', 'Error message did not appear.');
  }

  /**
   * Test with the setting enabled admin users can no longer sign in.
   */
  public function testStopAdminRoleSettingEnbledAdminUser() {
    \Drupal::configFactory()
      ->getEditable('stop_admin.settings')
      ->set('block_admin_role', TRUE)
      ->save(TRUE);

    $this->drupalGet('node');
    $edit = [
      'name' => $this->adminUser->getAccountName(),
      'pass' => $this->adminUser->passRaw,
    ];
    $this->submitForm($edit, 'Log in');
    $this->assertTrue(strpos($this->getSession()->getPage()->getContent(), (string) 'Log in') > 0, 'User is not logged in.');
    $this->assertSession()->pageTextContains('Unrecognized username or password. Forgot your password?', 'Error message appeared.');
  }

}
